#!/usr/bin/env python

import os
from functools import wraps

import speech_recognition as sr


class SpeechRecognition(object):

    @staticmethod
    def _validate_path(file_path):
        if not os.path.exists(file_path):
            raise FileExistsError

    @staticmethod
    def _check_microphone(microphone_index):
        try:
            microphone_name = sr.Microphone.list_microphone_names()[microphone_index]
        except IndexError:
            raise IOError("No valid microphone!")
        else:
            return microphone_name

    def __init__(self, preferred_engine='sphinx', user_id=None, pass_key=None,
                 language='en-US'):
        self.config = {
            'user_id': os.environ.get('USER_ID') or user_id,
            'pass_key': os.environ.get('PASS_KEY') or pass_key,
            'sr_engine': os.environ.get('SR_ENGINE') or preferred_engine,
            'sr_engine_lang': os.environ.get('SR_ENGINE_LANG') or language,
        }
        self.preferred_engine = preferred_engine
        self._r = sr.Recognizer()
        self.sr_engine = self.build_engine()

    def build_engine(self):
        user_id = self.config['user_id']
        pass_key = self.config['pass_key']
        preferred_engine = self.config['sr_engine']
        preferred_engine_lang = self.config['sr_engine_lang']
        sr_recognizers = {
            'bing': self._r.recognize_bing,
            'google': self._r.recognize_google,
            'google_cloud': self._r.recognize_google_cloud,
            'houndify': self._r.recognize_houndify,
            'ibm': self._r.recognize_ibm,
            'sphinx': self._r.recognize_sphinx,
            'wit': self._r.recognize_wit
        }
        engine = sr_recognizers[preferred_engine]

        def sr_engine(audio_data, language=preferred_engine_lang,
                      preferred_phrases=None, keyword_entries=None, grammar=None,
                      credentials_json=None, show_all=False):
            if preferred_engine in ('bing', 'google'):
                transcription = engine(audio_data, pass_key, language,
                                       show_all=show_all)
            elif preferred_engine == 'houndify':
                transcription = engine(audio_data, user_id, pass_key,
                                       show_all=show_all)
            elif preferred_engine == 'ibm':
                transcription = engine(audio_data, user_id, pass_key, language,
                                       show_all=show_all)
            elif preferred_engine == 'sphinx':
                transcription = engine(audio_data, language, show_all=show_all,
                                       keyword_entries=keyword_entries,
                                       grammar=grammar)
            elif preferred_engine == 'google_cloud':
                transcription = engine(audio_data, language, show_all=show_all,
                                       json_credentials=credentials_json,
                                       preferred_phrases=preferred_phrases)
            else:
                transcription = engine(audio_data, pass_key)

            return transcription
        return sr_engine

    def _parse_audio_data(self, audiodata):
        try:
            transcription = self.sr_engine(audiodata)
        except sr.UnknownValueError:
            raise sr.UnknownValueError("Speech is Unintelligible!")
        except sr.RequestError:
            raise sr.RequestError("Unable to reach remote engine")
        else:
            return transcription

    def transcribe_audio(self, audio_file_path, duration=None, offset=None,
                         reduce_noise=True):
        self._validate_path(audio_file_path)

        with sr.AudioFile(audio_file_path) as audio_file:
            if reduce_noise:
                self._r.adjust_for_ambient_noise(audio_file)
            audio = self._r.record(audio_file, duration, offset)

        return self._parse_audio_data(audio)

    def transcribe_record(self, microphone_index=None, reduce_noise=True,
                          timeout=60, phrase_limit=None):
        self._check_microphone(microphone_index)

        with sr.Microphone(device_index=microphone_index) as sound_source:
            if reduce_noise:
                self._r.adjust_for_ambient_noise(sound_source)

            audio = self._r.listen(
                sound_source, timeout=timeout, phrase_time_limit=phrase_limit)

        return self._parse_audio_data(audio)

    def background_transcribe_record(self, microphone_index=None,
                                     reduce_noise=True, phrase_limit=None):
        self._check_microphone(microphone_index)

        def decorator(func):
            @wraps(func)
            def wrapper(*args, **kwargs):
                def callback(recognizer_instance, audio_data):
                    transcription = self._parse_audio_data(audio_data)
                    return func(transcription=transcription, *args, **kwargs)

                with sr.Microphone(device_index=microphone_index) as sound_source:
                    if reduce_noise:
                        self._r.adjust_for_ambient_noise(sound_source)

                    record_stopper = self._r.listen_in_background(
                        sound_source, callback, phrase_time_limit=phrase_limit)
                return record_stopper
            return wrapper
        return decorator
