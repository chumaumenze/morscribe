#! /usr/bin/env python

from morscribe.constants import ITU_MORSE_CHARS


def text_to_morse_code(text_string, space_in_letters=1, space_in_words=4,
                       all_chars=False):
    morse_code = str()
    for char in text_string:
        morse_char = ITU_MORSE_CHARS.get(char) or str()
        if morse_char == '' and all_chars:
            morse_char = char + ' ' * space_in_letters
        elif morse_char == ' ':
            morse_char *= space_in_words
        else:
            morse_char += ' ' * space_in_letters

        morse_code += morse_char
    return morse_code.strip()


def morse_code_to_text(morse_code, space_in_words=4):
    text_string = str()
    morse_word_partition = morse_code.split(' ' * space_in_words)

    for morse_word in morse_word_partition:
        word_letters = str()

        for morse_char in morse_word.split():
            if morse_char == ' ':
                continue
            else:
                text_letter = None
                for text_value, code_value in ITU_MORSE_CHARS.items():
                    if code_value == morse_char:
                        text_letter = text_value
                        break
                word_letters += text_letter or morse_char

        text_string += word_letters + ' '

    return text_string.strip().upper()


if __name__ == '__main__':
    sample_morse_code = ".... . .-.. .-.. ---     .-- --- .-. .-.. -.. !     " \
                        ".... --- .--     .- .-. .     -.-- --- ..- ..--..".strip()
    sample_plain_text = "Hello World! How are you?"

    result_plain_text = morse_code_to_text(sample_morse_code)
    print("Morse2Plain:")
    print(sample_plain_text.upper())
    print(result_plain_text)

    result_morse_code = text_to_morse_code(sample_plain_text, all_chars=True)
    print("\n\nPlain2MOrse:")
    print(sample_morse_code)
    print(result_morse_code)

    print(
        "\n\nResult: \n"
        "Morse2Plain Passed: {}\n"
        "Plain2Morse Passed: {}\n".format(
            sample_plain_text.upper() == result_plain_text,
            sample_morse_code == result_morse_code
        )
    )
